import java.awt.Graphics;
import java.awt.Image;
import java.awt.Panel;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class GamePanelMap extends Panel {
	Karta karta;
	
	GamePanelMap(Karta k) {
		karta = k;
		
		GameKeyListener gkl = new GameKeyListener(this);
		addKeyListener(gkl);
		
		GameMouseListener gml = new GameMouseListener(this);
		addMouseListener(gml);	
	}
	
	public void paint(Graphics g) {
		BufferedImage img = null;
		File f = new File("point.jpg");
		File f1 = new File("assassindevil.jpg");
		try {
			img = ImageIO.read(f);
		} catch (IOException e) {
			e.printStackTrace();
		}
		g.drawImage(img, karta.killer.posx * 50, karta.killer.posy * 50, null);
		for (int i = 0; i < karta.sizex; i++) {
			for (int j = 0; j < karta.sizey; j++) {
				g.drawRect(i * 50, j * 50,  50,  50);
			}
		}
		
		try {
			img = ImageIO.read(f1);
		} catch (IOException e) {
			e.printStackTrace();
		}
		g.drawImage(img.getScaledInstance(50,50,Image.SCALE_FAST), karta.monster.posx * 50, karta.monster.posy * 50, null);
		
		for (int i = 0; i < karta.sizex; i++) {
			for (int j = 0; j < karta.sizey; j++) {
				g.drawRect(i * 50, j * 50,  50,  50);
			}
		}
	}
}
